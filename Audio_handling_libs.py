import pydub
from pydub import AudioSegment
import os 
from os import listdir, path
import shutil
import ffmpeg
import shutil
import eyed3
from multiprocessing import Process, current_process, Pool
import re
import datetime
import math
import jira
import pandas as pd
from jira.client import JIRA
import requests
from requests.auth import HTTPBasicAuth


def arkiv_restructure(directory): #remove previously converted audio files
    folders = listdir(directory)
    for folder in folders:
        if folder == "MP3" :
            shutil.rmtree(str(directory) + "\\" + str(folder))
        elif folder == "wav":
            shutil.rmtree(str(directory) + "\\" + str(folder))
        elif folder == "MP3 256":
            shutil.rmtree(str(directory) + "\\" + str(folder))                
        elif folder == "tagfile":
            if listdir(str(directory) + "\\" + str(folder)) == [] or listdir(str(directory) + "\\" + str(folder)) == ['Thumbs.db']:
                shutil.rmtree(str(directory) + "\\" + str(folder))
        elif folder == "Audiobook_cover" or folder == "audiobook_cover":
            if listdir(str(directory) + "\\" + str(folder)) == [] or listdir(str(directory) + "\\" + str(folder)) == ['Thumbs.db']:
                shutil.rmtree(str(directory) + "\\" + str(folder))
        else:
            pass
            
    for i in ["\\MP3","\\MP3 256","\\wav"]:
        try:
            os.mkdir(directory + i)
        except FileExistsError:
            pass


def check_audio_duration(directory, split_directory):
    dur_to_long = [] #list to hold files that are to long
    rms_level_adjust = []
    
    files = listdir(directory + "\\" + "Master") #get a list of all files in the master folder 
    for file in files:
            if file == "Thumbs.db" or file == "desktop.ini":
                os.remove(directory + "\\" + "Master" + "\\" + file)
            else:
                audio = AudioSegment.from_file(directory + "\\" + "Master" + "\\" + file)
                if audio.duration_seconds > 2699:
                    dur_to_long.append(file)
                else:
                    pass
                if audio.dBFS > -14 or audio.dBFS < -24:
                    rms_level_adjust.append(file)
                else:
                    pass

    return dur_to_long, rms_level_adjust

        
def make_archive_dorectories(directory):
    for i in ["\\MP3","\\MP3 256","\\wav"]:
        try:
            os.mkdir(directory + i)
        except FileExistsError:
            pass



def get_jira_information(si_number):
    jira = JIRA("https://jira.sagaegmont.com/",basic_auth=("EgmontBot","1997Norm"))# connect to jira api
    missing_jira_info = []
    isbn_num = jira.issue(si_number).raw["fields"]["customfield_10048"]
    language = jira.issue(si_number).raw["fields"]["customfield_10027"]["value"]
    try:
        artist = jira.issue(si_number).raw["fields"]["components"][0]["name"]
    except IndexError:
        components = []
        test = jira.issue(si_number).raw["fields"]["customfield_14012"]
        for t in test:
            mellem = re.findall("(.+?)\(", t)[0]
            components.append(mellem)
        artist = ",".join(components)
    if language in ["Arabic","Bengali","Bulgarian","Chinese","Greek","Hindi","Japanese","Korean","Marathi","Russian"]:
        title = jira.issue(si_number).raw["fields"]["customfield_13500"]
    else:
        title = jira.issue(si_number).fields.summary
    album = jira.issue(si_number).fields.summary
    try:
        if len(jira.issue(si_number).raw["fields"]["customfield_10612"]) == 1:
            comment = jira.issue(si_number).raw["fields"]["customfield_10612"][0]["value"]
        else:
            comment = ",".join(re.findall(r"value='(.+?)'",str(jira.issue(si_number).fields.customfield_10612)))
    except TypeError:
        comment = "missing"
        missing_jira_info.append(si_number + "No comment found in jira")
        
    return isbn_num, artist, title, album, comment, missing_jira_info, language

def get_cover_images(audio_directory, si_number, isbn):
    pic_not_there = []
    try:
        os.mkdir(audio_directory + "\\" + "tagfile")
        os.mkdir(audio_directory + "\\" + "Audiobook_cover")

        jira = JIRA("https://jira.sagaegmont.com/",basic_auth=("EgmontBot","1997Norm"))
        resp = requests.get('https://jira.sagaegmont.com/rest/attach-cat/1.0/attachments?issueKey='+si_number,auth=HTTPBasicAuth('EgmontBot', '1997Norm'))
        categories = resp.json()["categories"]
        for i in categories:
            if i["name"] == "Audiobook cover":
                try:
                    pic_id = i['attachmentIds'][0]
                except IndexError:
                    pic_not_there.append("Missing Audiobook Cover in jira")
        for i in categories:
            if i["name"] == "Tagfile":
                try:
                    tag_id = i['attachmentIds'][0]
                except IndexError:
                    pic_not_there.append("Missing Audiobook tagfile in jira")
        if pic_not_there == []:
            aud_cover = jira.attachment(pic_id).get()
            aud_tagfile = jira.attachment(tag_id).get()
            with open(audio_directory + "\\" + "tagfile" + "\\" + "tag.jpg", 'wb') as q:  #exporting the picture 
                q.write(aud_tagfile)
            with open(audio_directory + "\\" + "Audiobook_Cover" + "\\" + isbn + ".jpg", 'wb') as z:
                z.write(aud_cover)
        else:
            pass
            
    except FileExistsError:
        pass
    
    return pic_not_there



def convert_audio_files(audio_directory): #convert audio files to three formates (wav, mp3, mp3 256) and export to directories
    num = 0
    files = listdir(audio_directory + "\\" + "Master")
    duration = 0.0
    count = 0
    for file in files:
        num = num + 1
        if file.lower().endswith("mp3"):
            sound = AudioSegment.from_mp3(audio_directory + "\\Master\\" + file)
        elif file.lower().endswith("wav"):
            sound = AudioSegment.from_wav(audio_directory + "\\Master\\" + file)
            

        duration = duration + sound.duration_seconds
        count = count + 1
        sound = sound.set_channels(1)
        sound = sound.set_frame_rate(44100)
        sound_stereo = AudioSegment.from_mono_audiosegments(sound, sound)
        sound_stereo = sound_stereo.set_channels(2)
        sound_stereo = sound_stereo.set_frame_rate(44100)
        sound.export(audio_directory + "\\wav\\" + "{0:03}".format(num)+".wav", format="wav", bitrate="705k")
        sound.export(audio_directory + "\\MP3\\del" + "{0:03}".format(num)+".mp3", format="mp3", bitrate="96k")
        sound_stereo.export(audio_directory + "\\MP3 256\\" + "{0:03}".format(num) + ".mp3", format="mp3", bitrate="256k")
        
    return duration, count



def tag_audio_files(audio_directory, artist_value, album_value, comments_value, title_value, isbn_value):
    year = datetime.date.today().year
    tracks = listdir(audio_directory + "\\MP3")
    tagfile = listdir(audio_directory + "\\tagfile\\")
    
    for track in tracks:
        audiofile = eyed3.load(audio_directory + "\\MP3\\" + track)
        track_num = track.split(".")[0].replace("del","")
        audiofile.tag.images.set(3, open(audio_directory + "\\tagfile\\" + tagfile[0],'rb').read(), 'image/jpeg')
        audiofile.tag.artist = artist_value
        audiofile.tag.album = album_value
        audiofile.tag.comments.set(text=comments_value)
        audiofile.tag.genre = "Audiobook"
        audiofile.tag.track_num = (int(track_num), None)
        audiofile.tag.title = title_value + " - " + track_num
        audiofile.tag.recording_date = eyed3.core.Date(year)
        audiofile.tag.save(version=eyed3.id3.ID3_V2_3)
    shutil.make_archive(audio_directory + "\\" + isbn_value, 'zip', audio_directory + "\\MP3")




def rename_audio_directory(audio_directory, audiobook_title, isbn_num):
    rejected_charaters = "?/\|<>:*\"" # special charaters that cant be contained in a directory format
    specials = {"Å":"aa","Ø":"oe","Æ":"ae","Ö":"o","Ä":"a","Ü":"u","Í":"i","ñ":"n","ú":"u","é":"e","ó":"o","á":"a","à":"a","ê":"e"} # special letters that need to be changed
    
    for i in rejected_charaters: #loop for removing special characters 
        audiobook_title = audiobook_title.replace(i,"") 
        
    for special in specials.keys():
        if special in audiobook_title:
            audiobook_title = audiobook_title.replace(special,specials[special])
        elif special.lower() in audiobook_title:
            audiobook_title = audiobook_title.replace(special.lower(),specials[special])

    new_audiobook_directory = re.sub(r"S[A-Z]{1}\-\d+", "", audio_directory) + "\\" + audiobook_title + "_" + isbn_num
    
    
    return new_audiobook_directory
    


def send_email_notification(processed_audiobooks, audiobooks_split, process_duration, audiobooks_error):
    FROM = "lrirpa069@egmont.com"
    TO = ["daemnh@egmont.com", "robo-runmgmt@lrforlag.com"] #"jfo@lrforlag.dk"
    SUBJECT = "Daily Report " + str(datetime.datetime.today().date())
    SERVER = "smtp-relay.egmont.com"

    status = "Robot Run Status: "
    status_value = "Finished Processing"
    time = "Robot Processing Time: " + process_duration
    text1 = "The following audiobooks have been processed: "
    processed_audiobooks_list = "; ".join(processed_audiobooks)
    TEXT2 = "The following audiobooks contain files exceding the 45 min standard and have been split: "
    TEXT3 = "; ".join(audiobooks_split)
    text4 = "The following audiobooks were unable to be processed: "
    error_audiobooks_list = "; ".join(audiobooks_error)
    message = """From: %s\r\nTo: %s\r\nSubject: %s\r\n\

    %s
    %s
    
    %s
    
    %s
    %s
    
    %s
    %s

    %s
    %s
    
    """ % (FROM, ", ".join(TO), SUBJECT, status, status_value, time, text1, processed_audiobooks_list, TEXT2, TEXT3, text4, error_audiobooks_list)

    message = message.replace("-","")
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    print(message)
    server.sendmail(FROM, TO, message)
    server.quit()
        
    
def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


        
def split_audio_files(audio_directory, error_report):
    files = listdir(audio_directory + "\\Master")

    try:
        shutil.copytree(audio_directory + "\\Master", audio_directory + "\\Original")
    except FileExistsError:
        pass

    for file in files:
        if file.endswith(".mp3"):
            audio = AudioSegment.from_mp3(errors_path + folder + "\\" + "Master" + "\\" + file)
        elif file.endswith(".wav"):
            audio = AudioSegment.from_wav(errors_path + folder + "\\" + "Master" + "\\" + file)
        else:
            pass
        
        dur = audio.duration_seconds
        middel = dur/2
        slices = []

        if dur > 2699:
            split = pydub.silence.detect_silence(audio,min_silence_len=2000, silence_thresh=-36, seek_step=1)
            if split != []:
                for s in split:
                    slices.append(s[0]+(s[1]-s[0]))
                
                split_interval = find_nearest(slices, middel)
            
                first = audio[split_interval:]
                second = audio[:split_interval]
                first.export(audio_directory + "\\Master\\" + file.split(".mp3")[0]+"(1)"+".mp3", format="mp3")
                second.export(audio_directory + "\\Master\\" + file.split(".mp3")[0]+"(2)"+".mp3", format="mp3")
                os.remove(audio_directory + "\\Master\\" + file)
            else:
                shutil.move(audio_directory, error_report)
            
        else:
            pass

def set_jira_info(si_num, duration, count):
    jira = JIRA("https://jira.sagaegmont.com/",basic_auth=("EgmontBot","1997Norm"))# connect to jira api
    
    jira.issue(si_num).update(fields={"customfield_10605":int(str(duration/60/60).split(".")[0])}) # Runtime (hours)
    jira.issue(si_num).update(fields={"customfield_10600":int(float("0." + str(duration/60/60).split(".")[1])*60)}) #Runtime (minutes)
    jira.issue(si_num).update(fields={"customfield_13800":count}) # insert count


def splitting_audio_file(audio_directory, files):
    
    for file in files:
        if file.endswith(".mp3"):
            audio = AudioSegment.from_mp3(audio_directory + "\\" + "Master" + "\\" + file)
            file_format = "mp3"
        elif file.endswith(".wav"):
            audio = AudioSegment.from_wav(audio_directory + "\\" + "Master" + "\\" + file)
            file_format = "wav"
        else:
            pass

        count = 0
        while audio.duration_seconds > 2699:
            count = count + 1
            slices = []
            split = pydub.silence.detect_silence(audio,min_silence_len=2000, silence_thresh=-36, seek_step=1)
            if split != []:
                for s in split:
                    pause = (s[1]-s[0])/2
                    slices.append(s[0]+ pause)
    
            for slic in slices:
                if slic < 2699000:
                    split_interval = slic
                else:
                    pass
                    

            first = audio[:split_interval]
            second = audio[split_interval:]
            first.export(audio_directory + "\\Master\\" + file.split("."+file_format)[0]+"(" + str(count) + ")."+file_format, format=file_format)
            
            audio = second

        second.export(audio_directory + "\\Master\\" + file.split("."+file_format)[0]+"(" + str(count+1) + ")."+file_format, format=file_format)

        os.remove(audio_directory + "\\Master\\" + file)
            
            
    return slices, split_interval


def rms_level_adjustment(audio_directory, target_rms_level):
    try:
        #os.mkdir(audio_directory + "\\original")
        shutil.copytree(audio_directory + "\\master", audio_directory + "\\original")
    except FileExistsError:
        pass

    try:
        shutil.rmtree(audio_directory + "\\adjusted")
    except FileNotFoundError:
        pass
    
    os.mkdir(audio_directory + "\\adjusted")
    
    files = listdir(audio_directory + "\\original")
    for file in files:
        audio = AudioSegment.from_file(audio_directory + "\\original\\" + file)
        change = audio.dBFS - target_rms_level
        audio_adjusted = audio - change
        if file.endswith("mp3"):
            form = "mp3"
        elif file.endswith("wav"):
            form = "wav"
        
        audio_adjusted.export(audio_directory + "\\adjusted\\" + file, format= form )

    shutil.rmtree(audio_directory + "\\master")
    shutil.copytree(audio_directory + "\\adjusted", audio_directory + "\\master")
        
    
    
            
            
            
            
        
