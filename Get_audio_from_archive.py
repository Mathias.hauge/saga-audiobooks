#get jira issues
import jira
import pandas as pd
import re
from jira.client import JIRA
from os import listdir
import shutil
import os
import smtplib
import datetime

start = datetime.datetime.now()


jira = JIRA("https://jira.sagaegmont.com/",basic_auth=("EgmontBot","1997Norm"))
issues_si = jira.search_issues("project=SI", maxResults=False,startAt=1)
issues_sb = jira.search_issues("project=SB", maxResults=False,startAt=1)
issues_si.extend(issues_sb)
all_issues = issues_si
#notes:
#issue.key, 0                                             #SI or SB number
#issue.fields.summary, 1                                  # title 
#issue.raw["fields"]["customfield_10048"], 2              #ISBN number 
#issue.raw["fields"]["customfield_10027"]["value"], 3     #language
#issue.raw["fields"]["customfield_14012"], 4              #Author (Insight):
#issue.raw["fields"]["customfield_13500"], 5              #latin title
#issue.raw["fields"]["customfield_10612"], 6              #Audiobook narrator
#issue.raw["fields"]["components"], 7                     #components 
#issue.raw["fields"]["customfield_10088"], 8              #meta data approved
#issue.raw["fields"]["customfield_10334"], 9              #Author rights accepted (T)
#issue.raw["fields"]["customfield_10333"], 10             #author rights
#issue.raw["fields"]["customfield_10019"], 11             #translator rights
#issue.raw["fields"]["customfield_10822"] 12              #Audiobook cover 3000x3000
#issue.raw["fields"]["customfield_10377"] 13              #New master status:
#issue.raw["fields"]["customfield_10351"] 14              #Author rights accepted (O)
#issue.raw["fields"]["customfield_10072"] 15              #Master Source
#issue.raw["fields"]["customfield_10368"] 15              #Remastering status
#issue.raw["fields"]["customfield_10022"] 16              #Translator rights - accepted
#issue.raw["fields"]["customfield_10821"] 17              #Ebook cover 2400x3200
#issue.raw["fields"]["customfield_14403"] 18              #SLX Status Audio
#issue.raw["fields"]["customfield_14006"] 19              #Cover rework
#issue.raw["fields"]["customfield_14007"] 20              #Meta rework
new_db = []
for issue in all_issues:
    try:
        new_db.append([issue.key, issue.fields.summary, issue.raw["fields"]["customfield_10048"], issue.raw["fields"]["customfield_10027"], issue.raw["fields"]["customfield_14012"], issue.raw["fields"]["customfield_13500"], issue.raw["fields"]["customfield_10612"], issue.raw["fields"]["components"], issue.raw["fields"]["customfield_10088"], issue.raw["fields"]["customfield_10334"], issue.raw["fields"]["customfield_10333"], issue.raw["fields"]["customfield_10019"], issue.raw["fields"]["customfield_10822"], issue.raw["fields"]["customfield_10377"], issue.raw["fields"]["customfield_10351"], issue.raw["fields"]["customfield_10072"], issue.raw["fields"]["customfield_10368"], issue.raw["fields"]["customfield_10022"], issue.raw["fields"]["customfield_10821"],issue.raw["fields"]["customfield_14403"],issue.raw["fields"]["customfield_14006"], issue.raw["fields"]["customfield_14007"]])
    except IndexError and TypeError:
        pass


#filter for BIB audio and Dcbelle
#filtering of jira data
df = pd.DataFrame(new_db, columns=["SI/SB", "Title", "ISBN Number", "Language", "Author (insight)", "Latin Ttile", "Audiobook narrator", "Components", "Metadata status", "Author rights accepted (T)", "Author Rights 2", "Translator Rights", "Audiobook cover 3000x3000", "New master status", "Author rights accepted (O)", "Master Source","Remastering status", "Translator rights - accepted", "Ebook cover 2400x3200", "SLX Status Audio", "Cover Rework", "Meta rework"])
df = df.fillna("nan")
#final_df = df.loc[df["Remastering status"] == "Ready for remastering"]
final_df = df.loc[df["ISBN Number"]!="nan"]
final_df = final_df.loc[(final_df["Author rights accepted (O)"].astype(str).str.contains("Audiobook")) | (final_df["Author rights accepted (T)"].astype(str).str.contains("Audiobook") & final_df["Translator rights - accepted"].astype(str).str.contains("Audiobook"))]
#final_df = final_df.loc[final_df["Translator rights - accepted"].astype(str).str.contains("Audiobook")]
final_df = final_df.loc[(final_df["Audiobook cover 3000x3000"].astype(str).str.contains("Approved"))]
final_df = final_df.loc[(final_df["Master Source"].astype(str).str.contains("BibAudio"))|(final_df["Master Source"].astype(str).str.contains("dcBelle"))]
final_df = final_df.loc[final_df["Metadata status"].astype(str).str.contains("Metadata approved")]
final_df = final_df.loc[final_df["Audiobook narrator"]!="nan"]
final_df = final_df.loc[final_df["New master status"].astype(str).str.contains("New master delivered")]
findal_df = final_df.loc[(final_df["Cover Rework"] != "nan")&(~final_df["Cover Rework"].astype(str).str.contains("Audiobook"))]
findal_df = final_df.loc[(final_df["Meta rework"] != "nan")&(~final_df["Meta rework"].astype(str).str.contains("Audiobook"))]
final_df = final_df.loc[(final_df["SLX Status Audio"].astype(str).str.contains("Finished")) | (final_df["SLX Status Audio"].astype(str).str.contains("Accepted"))]

#final_df = final_df.loc[final_df["Master Source"]!="nan"]
si_nums = list(final_df["SI/SB"])

# look for audio book in ftp 
remastering_dir = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\1 READY FOR REMASTERING"
directories = []
f = []
top = []
move = []
not_move = []
charas = ["\\","/","?",":","|","<",">"]
for root, dirs, files in os.walk(r"\\egmont.com\fs\lri_archive\AudioBooks\Production\0 FTP\in"):
    top.append(root)
    directories.append(dirs)
    f.append(files)


for i in top:
    for t in si_nums:
        for chara in charas:
            t.replace(chara,"")
        if t == i.split("\\")[-1]:
            move.append(i)
        else:
            if t not in not_move:
                not_move.append(t)


for folder in move:
    shutil.move(folder, remastering_dir)
    print(folder + " moved")
    

finish = datetime.datetime.now()
process_duration = str(finish - start)


#send email with all found and not found audio books 
FROM = "lrirpa069@egmont.com"
TO = ["daemnh@egmont.com", "robo-runmgmt@lrforlag.com"] #"jfo@lrforlag.dk"
SUBJECT = "Jira Filtering " + str(datetime.datetime.today().date())
SERVER = "smtp-relay.egmont.com"

status = "Robot Run Status: "
status_value = "Finished Processing"
time = "Robot Processing Time: " + process_duration
text1 = "The following audiobooks have been found and moved: "
processed_audiobooks_list = "; ".join(move)
TEXT2 = "The following audiobooks are ready but have not been moved to robot: "
TEXT3 = "; ".join(not_move)
message = """From: %s\r\nTo: %s\r\nSubject: %s\r\n\

%s
%s
    
%s
    
%s
%s
    
%s
%s

    
""" % (FROM, ", ".join(TO), SUBJECT, status, status_value, time, text1, processed_audiobooks_list, TEXT2, TEXT3)

message = message.replace("-","")
# Send the mail
server = smtplib.SMTP(SERVER)
print(message)
server.sendmail(FROM, TO, message)
server.quit()
