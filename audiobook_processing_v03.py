from Audio_handling_libs import *

def audiobook_processing(audio_book):
        ready_for_remastering = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\1 READY FOR REMASTERING"

        splitting_in_progress = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\1.5 SPlITTING IN PROGRESS"

        error_report = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\3 ERROR REPORT"

        remastering_done = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\2 REMASTER DONE"
    
        audiobook_dir = ready_for_remastering + "\\" + audio_book
        split_files, rms_level_adjust = check_audio_duration(audiobook_dir, splitting_in_progress) # check if the audio tracks are within standards
        print(split_files, rms_level_adjust)

        audiobook_splitted, audiobook_adjusted = "None", "None"
        error_info = "None"

        
        if split_files != []:
            audiobook_splitted = audio_book
            splitting_audio_file(audiobook_dir, split_files)
        else:
            pass
        if rms_level_adjust != []:
                audiobook_adjusted = audio_book
                rms_level_adjustment(audiobook_dir, -20)
        else:
                pass
                    
        arkiv_restructure(audiobook_dir) # delete irrelevant directoryies and create relevant 
                
        duration, count = convert_audio_files(audiobook_dir) # convert audio files into three different formats
                        
        isbn_num, artist, audiobook_title, album, comment, missing_jira_info, language = get_jira_information(audio_book) # retrieve information need to tag each track
        if missing_jira_info == []:
                error_info = "None"
                pic_not_there = get_cover_images(audiobook_dir, audio_book, isbn_num) #get tag file and cover file

                if pic_not_there != []:
                        error_info = audiobook_title + " " + ", ".join(pic_not_there)
                        shutil.move(ready_for_remastering + "\\" + audio_book, error_report)
                else:        
                        tag_audio_files(audiobook_dir, artist, album, comment, audiobook_title, isbn_num) # tag each track with meta data and jpeg

                        new_audiobook_directory = rename_audio_directory(audiobook_dir, audiobook_title, isbn_num) # rename the aduio book directory to be title + isbn number and move to remaster done

                        set_jira_info(audio_book, duration, count)
                        try:
                                os.rename(audiobook_dir, new_audiobook_directory)
                        except PermissionError:
                                pass
                        
                try:
                        shutil.move(ready_for_remastering + "\\" + re.findall(r"READY FOR REMASTERING\\(.+?\_\d{13})",new_audiobook_directory)[0], remastering_done + "\\" + language) # move to remaster done folder
                except:
                        pass
        else:
                error_info = missing_jira_info[0]
                shutil.move(audiobook_dir, error_report)


        return error_info, audiobook_title, audiobook_splitted, audiobook_adjusted, isbn_num


if __name__=='__main__':
    ready_for_remastering = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\1 READY FOR REMASTERING"
    start = datetime.datetime.now()

    splitting_in_progress = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\1.5 SPlITTING IN PROGRESS"

    error_report = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\3 ERROR REPORT"

    remastering_done = r"\\egmont.com\fs\lri_archive\AudioBooks\Production\1 Remastering\2 REMASTER DONE"

    audio_books = listdir(ready_for_remastering)
    audio_books.remove("Tagging data.xlsx")

    processed_audiobooks = []
    audiobooks_parted = []
    audiobooks_audio_adjustments = []
    error_audiobooks = []
    audiobooks_for_processing = []
    for audio_book in audio_books:
        if "SI" in audio_book or "SB" in audio_book or "SD" in audio_book:
                print(audio_book)
                audiobooks_for_processing.append(audio_book)
            

    pool = Pool(processes=6)
    information = pool.map(audiobook_processing, audiobooks_for_processing)
    pool.close()
    pool.join()
    print("finish")

    finish = datetime.datetime.now()
    process_duration = str(finish - start)
    print(process_duration)
    for i in information:
            print(i)
            processed_audiobooks.append(" ".join(re.findall(r"\b[A-Za-z]+\b",i[1]))+ "_" + str(i[4]))
            audiobooks_parted.append(" ".join(re.findall(r"\b[A-Za-z]+\b",i[2])))
            audiobooks_audio_adjustments.append(" ".join(re.findall(r"\b[A-Za-z]+\b",i[3])))
            error_audiobooks.append(" ".join(re.findall(r"\b[A-Za-z]+\b",i[0])))

    print(processed_audiobooks)    
    send_email_notification(processed_audiobooks, set(audiobooks_parted), process_duration, set(error_audiobooks))
    
